# FlipDot Client / Server

HTTP-Server for a FlipDot Display (which needs to be connected by I2C). The following Hardware is needed:

## Hardware 

As a client connected as I2C Slave:
- an Arduino nano...
- ... mounted on the circuitboard http://rainer.radow.org/flip-dot-wwflip.php...
- ... controlling a FlipDot-Display
- (or any other I2C-Client if you code a I2C-framebuffer on your own)

```
Preferences in the Arduino-IDE:
     Board: Arduino Nano,
     Processor: ATMega 328P (old Bootloader)

Wiring (I2C default for nano):
     SDA: A4 (I2C Data)
     SCL: A5 (I2C Clock)
     GND: GND of I2C-Master
```
As a Server:
- a nodeMCU board (with few changes in code: any ESP8266-Board)

```
Preferences in the Arduino-IDE:
   Board: nodeMCU 1.0

Wiring (I2C default for nodeMCU):
       SDA: D2 (I2C Data)
       SCL: D1 (I2C Clock)
       GND: GND of I2C-Slave
```

## Software

Both Client and Server make use of (and are partly copied from) wwFlip-Library of Rainer Radow
            http://rainer.radow.org/flip-dot-wwflip.php
           (c) by Rainer Radow, 30171 Hannover, Germany
               radow.org

## Configuration for your Display

Besides the configuration of the wwFlip-Library you need to change (both client and server):

```
const int DISPLAY_WIDTH = 28;
const int DISPLAY_HEIGHT = 16;
```

## Configuration for your WiFi

WiFi-credentials need to be configured in the "credentials.h"-file to keep them out of the repository (use .gitignore or `git update-index --assume-unchanged src/FlipDotWebserver/credentials.h` for that.).

Credentials might be added to these two lines of Webserver, if you like it quick & dirty:

```
const char* WIFI_SSID     = WIFI_SSID_HIDDEN; /*Replace with WiFi-Credentials if you want to place them here */
const char* WIFI_PRESHAREDKEY = WIFI_PRESHAREDKEY_HIDDEN; /*Replace with WiFi-Credentials if you want to place them here */
```

## How to start

When connected to WiFi the IP-Address should appear on the FlipDot-Display (if you use Serial Monitor it should be logged as well) - use a browser to send messages!
