/*

     I2C-Client controlling a FlipDot-Display using wwFlip-Library of Rainer Radow
              http://rainer.radow.org/flip-dot-wwflip.php
             (c) by Rainer Radow, 30171 Hannover, Germany
                 radow.org

     Preferences in the Arduino-IDE:
     Board: Arduino Nano,
     Processor: ATMega 328P (old Bootloader)
     Wiring (I2C default for nano):
     SDA: A4 (I2C Data)
     SCL: A5 (I2C Clock)
     GND: GND of I2C-Master
*/


/*
   Debugging-Flags (enables Output on Serial Monitor)
   uncomment DEBUG plus the needed options
*/
//#define DEBUG 1
/* the following flags allow to get precise Information
    DEBUG-Flag is needed anyway to get Serial to work
    uncomment to get more Info
*/
//#define DEBUG_DataReceived 1
//#define DEBUG_NO_MATRIX_OUTPUT 1 /* Framebuffer-Content will be promptet on Serial Monitor */
//#define DEBUG_SHOWFRAME


/*
   Library for conectivity to FlipDot-Display
   http://rainer.radow.org/flip-dot-wwflip.php
*/
#include "wwFlip.h"
wwFlip ww;
const int DISPLAY_WIDTH = 28;
const int DISPLAY_HEIGHT = 16;
const byte WAIT_UNTIL_FLIPPED = 2;  /* looks nice, if there's some time-lag between two flipping dots */
const bool AT_ONCE = true;  /* appearing dot by dot or all dots at once? */

/*
  Use Byte-Array as Framebuffer
  Display size needed to calculate the Array-Size
*/
const byte SIZE_OF_FRAMEBUFFER = DISPLAY_WIDTH * DISPLAY_HEIGHT / 8;
byte framebuffer[SIZE_OF_FRAMEBUFFER];

/*
   I2C Config
   Wireing see above
*/
#include <Wire.h>
const int SLAVE_ADR = 8;
const byte MAX_BYTE_SENT_PER_I2C = 30; /* 32 Byte minus 2 for Totalnumber & Number */

byte messageReceived = 0; /* Parts of I2C-Messages that were allready received
                          (Multipart-Messages bc just 32 Bytes per I2C-Message)*/


bool isNewFrameAvailableToBeDisplayed = false; /* Flag set if all I2C messages of Frame are received */

void setup() {
#ifdef DEBUG
  Serial.begin(9600);           /  /* For Debugging with Serial-Monitor of Arduino IDE */
#endif

  Wire.begin(SLAVE_ADR);                /* join i2c bus with address SLAVE_ADR (should be 8), see above for wiring information*/
  Wire.onReceive(receiveEvent);         /* register receive event : call function receiveEvent */

  /* Config for the wwFlip-Library */
  ww.begin();
  ww.dotPowerOn();    /* needed for the FlipDots to work */
  ww.setCoilResetDuration(800);
  ww.setCoilSetDuration(500);
  ww.setAll(1);     /* Check first, if everything turns yellow */
  ww.resetAll(1);   /* turn black again */

  /* Set Framebuffer-Array-Bytes to 0b00000000 */
  fillByteArray(framebuffer, SIZE_OF_FRAMEBUFFER, 0);
}

void loop() {
  delay(100); /* Have a sleep... until a Frame appears... */

#ifdef DEBUG
  Serial.print("."); /* Make "I'm still working" visible */
#endif

  if (isNewFrameAvailableToBeDisplayed) { /*received a new Frame!!! */
    showFrame(); /* Display Framebuffer on FlipDot */
  }
}

/*
  Function called each time a Message is send to Clients Address
  from Master.
*/
void receiveEvent(int howMany) {

  byte messageNr = Wire.read();
  byte messagesTotal = Wire.read();
  byte packageCount = (messageNr - 1) * MAX_BYTE_SENT_PER_I2C;

#ifdef DEBUG
  Serial.println();
  Serial.print("Empfange die Nachricht: ");
  Serial.print(messageNr);
  Serial.print(" von ");
  Serial.print(messagesTotal);
  Serial.print(" starte ab Byte-Paket ");
  Serial.print(packageCount);
  Serial.println(" (DEBUG_DataReceived für mehr Infos aktivieren)");

#endif

  if (messageNr == 1) {
    messageReceived = 1;

#ifdef DEBUG_DataReceived
    Serial.println("Die erste Nachricht ist angekommen!");
#endif

  } else if (messageNr == messageReceived + 1 ) {
    messageReceived = messageNr;

#ifdef DEBUG_DataReceived
    Serial.println("Eine weitere Nachricht ist angekommen!");
#endif

  } else {
    messageReceived = 0;

#ifdef DEBUG_DataReceived
    Serial.println("Fehler in der Übertragung - Nachrichten gelöscht.");
#endif

  }

  while (0 < Wire.available()) {
    byte data = Wire.read();
    if (messageReceived != 0) {

#ifdef DEBUG_DataReceived
      Serial.print(data, BIN);
      Serial.print("|(");
      Serial.print(packageCount);
      Serial.print("|");
      Serial.print(SIZE_OF_FRAMEBUFFER);
      Serial.println(")");
#endif

      if (packageCount < (SIZE_OF_FRAMEBUFFER)) {
        framebuffer[packageCount] = data;
        packageCount++;
      } else {
        packageCount = 0;
      }
    }
  }

  if (messageReceived == messagesTotal) {
    isNewFrameAvailableToBeDisplayed = true;
    messageReceived = 0;

#ifdef DEBUG_DataReceived
    Serial.println("Alle Nachrichten angekommen!");
#endif

  }

#ifdef DEBUG_DataReceived
  Serial.println("EOF");
#endif

}

/*
   Set all bytes to 0b00000000 at beginning
   or to other values for testing reasons
*/

void fillByteArray(byte byteArray[], byte sizeOfArray, byte fillWith) {
  for (byte i = 0; i < (sizeOfArray); i++) {
    if (fillWith == -1) {
      byteArray[i] = i;
    } else {
      byteArray[i] = fillWith;
    }
  }
}

/*
   Parse the Framebuffer-Array and call the wwFlip
   Functions to physically start flipping...
*/

void showFrame(bool atOnce) {
  bool bitValue = false;
  byte xPos = 0;
  byte yPos = 0;
  isNewFrameAvailableToBeDisplayed = false;

#ifdef DEBUG
  Serial.println();
  Serial.print("Ausgabe eines neuen Frameworks (DEBUG_SHOWFRAME für mehr Infos aktivieren)");
#endif

#ifdef DEBUG_SHOWFRAME
  printFramebuffer();
#endif

#ifdef DEBUG_NO_MATRIX_OUTPUT
  printFramebuffer();
#endif

  for (byte byteNr = 0; byteNr < SIZE_OF_FRAMEBUFFER; byteNr++) {
    for (byte bitNr = 0; bitNr < 8; bitNr ++) { /* set bit by bit */
      bitValue = readBitInByte(framebuffer[byteNr], bitNr);
      xPos = 1 + ((float)byteNr * 8 + bitNr) / DISPLAY_HEIGHT;
      yPos = 1 + (byteNr * 8 + bitNr) % DISPLAY_HEIGHT;

#ifdef DEBUG_SHOWFRAME
      Serial.print("New Value: (");
      Serial.print(xPos);
      Serial.print(" / ");
      Serial.print(yPos);
      Serial.print(") =  ");
      Serial.println(bitValue);
#endif

#ifndef DEBUG_NO_MATRIX_OUTPUT
      if (bitValue == true) {
        if (atOnce) {
          ww.mSetDot(xPos , yPos );
        } else {
          ww.setDot(xPos , yPos );
        }
      } else {
        if (atOnce) {
          ww.mResetDot(xPos , yPos);
        } else {
          ww.resetDot(xPos, yPos );
        }
      }
      if (!atOnce) {
        delay(WAIT_UNTIL_FLIPPED);
      }
#endif

    }
  }

  if (atOnce) {
    ww.mUpdate();
    delay(WAIT_UNTIL_FLIPPED);
  }
}

/*
   Overloaded Version of showFrame, using default behavior
   for "atOnce"
*/
void showFrame() {
  showFrame(AT_ONCE);
}

/*
   Get the state of each dot by coordinates
   to be able to (debug-)print the Framebuffers content
*/
bool readFramebufferBit(byte x, byte y) {
  byte arrayPos = floor(((float)DISPLAY_HEIGHT * x + y) / 8);
  byte bitPos = (DISPLAY_HEIGHT * x + y) % 8;
  return readBitInByte(framebuffer[arrayPos], bitPos);
}


/*
   Print Framebuffer-content on Serial-Monitor
   Serial has to be startet i.e. DEBUG-Flag must be set
*/
void printFramebuffer() {
#ifdef DEBUG
  Serial.println("Darstellung des aktuellen Framebuffers: ");
  for (byte y = 0; y < DISPLAY_HEIGHT; y++) {
    for (byte x = 0; x < DISPLAY_WIDTH; x++) {
      Serial.print(readFramebufferBit(x, y) ? "X" : " ");
    }
    Serial.println();
  }
#endif
}

/*
   Short-ciruit to evaluate the relevant
   bits value in (Frambuffers) Byte
*/
bool readBitInByte(byte value, byte bitNr) {
  return (((value) >> (bitNr)) & 0x01);
}
