/* 
 * This file should keep the actual WiFi credentials
 * an should not be added to repository or any ZIP-File
 * 
 * I used the "git update-index --assume-unchanged src/wwFlip-i2c-byte-master/credentials.h" command as mentioned in
 * these hints on how to hide the credentials
 * 
 * https://arduino.stackexchange.com/questions/40411/hiding-wlan-password-when-pushing-to-github
 */

#ifndef CREDENTIALS_H
#define CREDENTIALS_H

// Replace with your actual SSID and password:
#define WIFI_SSID_HIDDEN "WHATS YOUR WIFI CALLED?"
#define WIFI_PRESHAREDKEY_HIDDEN "TYPE YOUR PSK HERE"

#endif
