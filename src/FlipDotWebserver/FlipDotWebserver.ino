/*
  HTTP-Server for a FlipDot Display (which needs to be connected by I2C)
  Mainly copied from different sources, mentioned in the comments below.
  Arranged by Hannes Stein

  Post textmessages to this HTTP-Server, it will generate a Framebuffer-Byte-Array
  and send it via I2C to a connected Client (in this case: the FlipDot Display)

  This Source makes use of (partly copied) and was inspired by wwFlip-Library of Rainer Radow
            http://rainer.radow.org/flip-dot-wwflip.php
           (c) by Rainer Radow, 30171 Hannover, Germany
               radow.org

   Preferences in the Arduino-IDE:
   Board: nodeMCU 1.0
   Wiring (I2C default for nodeMCU):
       SDA: D2 (I2C Data)
       SCL: D1 (I2C Clock)
       GND: GND of I2C-Slave

   WiFi-credentials need to be configured in the "credentials.h"-file or in the
   section below (WIFI_SSID /WIFI_PRESHAREDKEY
*/


/*
   Debugging-Flags (enables Output on Serial Monitor)
   uncomment DEBUG plus the needed options
*/
#define DEBUG 1
/* the following flags allow to get precise Information
    DEBUG-Flag is needed anyway to get Serial to work
    uncomment to get more Info
*/
//# define DEBUG_FONT 1
//# define DEBUG_FRAMBUFFER 1
//# define DEBUG_SEND_FRAMEBUFFER 1
//# define DEBUG_CHAR 1
//# define DEBUG_WIFI 1
//# define DEBUG_WIFI_EXTREME 1
//# define DEBUG_HTTP 1

/*
  Use Byte-Array as Framebuffer
  Display size needed to calculate the Array-Size
*/
const int DISPLAY_WIDTH = 28;
const int DISPLAY_HEIGHT = 16;
const byte SIZE_OF_FRAMEBUFFER = DISPLAY_WIDTH * DISPLAY_HEIGHT / 8;
byte framebuffer[SIZE_OF_FRAMEBUFFER];

/*
   I2C Config
   Wireing see above
*/
#include <Wire.h>
const int SLAVE_ADR = 8;
const byte MAX_BYTE_SENT_PER_I2C = 30; /* 32 Byte minus 2 for Totalnumber & Number */
const byte delayForI2C = 20;

/*
   Fonts to be parsed
   FontSize needed to calculate Displayposition

   Font-Processing copied and modified from Library for conectivity to FlipDot-Display
   http://rainer.radow.org/flip-dot-wwflip.php
*/
#include "font5x8_small.h"
byte gFontStartsAt = 0;
byte gFontEndsAt;
byte gFontHeight;
byte gFontWidth;
byte gFontCharacters;


/*
   WiFi Connection
*/
//Constants used for WIFI-connectivity to an existing WIFI-Accesspoint
#include "credentials.h"  /* include external file with WiFi credentials, uncomment if you want to type it directly */
const char* WIFI_SSID     = WIFI_SSID_HIDDEN; /*Replace with WiFi-Credentials if you want to place them here */
const char* WIFI_PRESHAREDKEY = WIFI_PRESHAREDKEY_HIDDEN; /*Replace with WiFi-Credentials if you want to place them here */
const char* WIFI_HOSTNAME = "FlipDotServer"; //must be unique, defautls to ESP+MAC
const int WIFI_RETRY_DELAY = 500;
const int MAX_WIFI_INIT_RETRY = 50;

/*
   HTTP-Server
*/
#include <ESP8266WebServer.h>
ESP8266WebServer httpRestServer(80);


String gTextToBeDisplayed = ""; /* This content will be sent to Display */

void setup() {

#ifdef DEBUG
  Serial.begin(9600);   /* For Debugging with Serial-Monitor of Arduino IDE */
#endif

  Wire.setClock(10000); /* I had issues with faster I2C - so I dropped the speed */
  Wire.begin(D1, D2); /* join i2c bus with SDA=D1 and SCL=D2 of NodeMCU */

  /* Set Framebuffer-Array-Bytes to 0b00000000 */
  fillByteArray(0, SIZE_OF_FRAMEBUFFER);

  parseFont();   /* needed to know the fonts characteristics */

  if (connectToWIFI() == WL_CONNECTED) {
#ifdef DEBUG
    Serial.print("WIFI connected ");
    gTextToBeDisplayed = "Browse ";
    gTextToBeDisplayed = gTextToBeDisplayed + WiFi.localIP().toString().c_str();;
#endif
  } else {
    gTextToBeDisplayed = "no WiFi";
    delay(5000);
#ifdef DEBUG
    Serial.print("WIFI not connected ");
#endif
  }

  configurateRestServerRouting();
  httpRestServer.begin();


}


void loop() {
  httpRestServer.handleClient();
  textPager(gTextToBeDisplayed, 2000, 0, true);
#ifdef DEBUG
  Serial.print(".");   /* Still alive dot */
#endif
  delay(3000);
}


/*
   Set all bytes to 0b00000000 at beginning
   or to other values for testing reasons
*/
void fillByteArray(byte fillWith, byte sizeOfArray) {
  for (byte i = 0; i < sizeOfArray; i++) {
    if (fillWith == -1) {
      framebuffer[i] = i;
    } else {
      framebuffer[i] = fillWith;
    }
  }
}

// ===========================================================================================
/* Open I2C Transmission, split the Framebuffer-Array into
    multiple messages, paginate and send each message
    individually
*/
void sendFrameBuffer(byte byteArray[], byte sizeOfArray) {
  byte messageNr = 1;
  byte byteCount = 0;
  byte messagesTotal = ceil((float)sizeOfArray / MAX_BYTE_SENT_PER_I2C);

  beginMessage(messageNr, messagesTotal, false);

#ifdef DEBUG_SEND_FRAMEBUFFER
  Serial.print("Sende den Framebuffer mit Anzahl an Elementen: ");
  Serial.print(sizeOfArray);
  Serial.println(" weitere Infos mit der Flag DEBUG_SEND_FRAMEBUFFER");
#endif

  for (byte i = 0; i < sizeOfArray; i++) {
    Wire.write(byteArray[i]);  //data bytes are queued in local buffer
    byteCount++;
    if ((byteCount >= MAX_BYTE_SENT_PER_I2C) && (i + 1 < sizeOfArray)) {
      messageNr++;
      beginMessage(messageNr, messagesTotal, true);

#ifdef DEBUG_SEND_FRAMEBUFFER
      Serial.print("Neue Nachricht, da bereits: ");
      Serial.print(byteCount);
      Serial.print(" Byte gesendet und noch ");
      Serial.print(sizeOfArray - (i + 1));
      Serial.println(" ausstehend.");
#endif

      byteCount = 0;
    }
  }
  Wire.endTransmission(SLAVE_ADR);    /* stop transmitting */
  delay(delayForI2C);

#ifdef DEBUG_FRAMBUFFER
  printFramebuffer();
#endif
}

/*
   Prepare each Transmission
   (Close old one, open new Transmission
*/
void beginMessage(byte messageNr, byte messagesTotal, bool endFormerTransmission) {
  if (endFormerTransmission) {
    Wire.endTransmission(SLAVE_ADR);    /* stop transmitting */
    delay(delayForI2C);
  }
#ifdef DEBUG_SEND_FRAMEBUFFER
  Serial.print("Sende die Nachricht: ");
  Serial.print(messageNr);
  Serial.print(" von ");
  Serial.println(messagesTotal);
#endif

  delay(delayForI2C);
  Wire.beginTransmission(SLAVE_ADR);
  Wire.write(messageNr);
  Wire.write(messagesTotal);
}

// ===========================================================================================
/*
   Handy Handling of the Framebuffer-Array
*/
void setFramebufferBit(byte x, byte y, bool isSet) {
  byte arrayPos = floor(((float)DISPLAY_HEIGHT * x + y) / 8);
  byte bitPos = (DISPLAY_HEIGHT * x + y) % 8;
  framebuffer[arrayPos] = writeBitInByte(framebuffer[arrayPos], bitPos, isSet);
}

/*
   Overloaded Version of setFramebufferBit(..)
*/
void setFramebufferBit(byte x, byte y) {
  setFramebufferBit(x, y, true);
}

/*
   Overloaded Version of setFramebufferBit(..)
*/
void resetFramebufferBit(byte x, byte y) {
  setFramebufferBit(x, y, false);
}

/*
   Get the state of each dot by coordinates
   to be able to (debug-)print the Framebuffers content
*/
bool readFramebufferBit(byte x, byte y) {
  byte arrayPos = floor(((float)DISPLAY_HEIGHT * x + y) / 8);
  byte bitPos = (DISPLAY_HEIGHT * x + y) % 8;
  return readBitInByte(framebuffer[arrayPos], bitPos);
}

/*
   Print Framebuffer-content on Serial-Monitor
   Serial has to be startet i.e. DEBUG-Flag must be set
*/
void printFramebuffer() {
#ifdef DEBUG
  Serial.println("Darstellung des aktuellen Framebuffers: ");
  for (byte y = 0; y < DISPLAY_HEIGHT; y++) {
    for (byte x = 0; x < DISPLAY_WIDTH; x++) {
      Serial.print(readFramebufferBit(x, y) ? "X" : " ");
    }
    Serial.println();
  }
#endif
}


// ===========================================================================================
/*
   Parse Text Character by Character and revoke Methods to
   display the Fonts on FlipDot-Screen
   (Method mostly copied of wwFlip-Library-Examples)
*/
void textPager(String text, int milisNextPage, int charDelay, bool atOnce, int xCursor, int yCursor) {
  byte charStartsAt = 0;
  byte charEndsAt = 0;
  byte charWidth = 0;

  fillByteArray(0, SIZE_OF_FRAMEBUFFER);

  for (int i = 0; text[i] != 0; i++) {
    charStartsAt = 0;
    charEndsAt = 0;
    charWidth = charSize(text[i],  charStartsAt, charEndsAt); //parse this character and get it's specific start, end and width

    if (xCursor + charWidth - 1 > DISPLAY_WIDTH) { // char doesn't fit row
      yCursor =  yCursor + gFontHeight ;     // new line
      xCursor = 1;                          // start at pos 1

      if (yCursor + gFontHeight - 1 > DISPLAY_HEIGHT) { // row doesn't fit screen?
        if (atOnce) {
          sendFrameBuffer(framebuffer, SIZE_OF_FRAMEBUFFER);
        }
        delay(milisNextPage);
        fillByteArray(0, SIZE_OF_FRAMEBUFFER);
        xCursor = 1;                    //start at pos 1
        yCursor = 1;
      }
    }
    xCursor = drawChar(text[i], xCursor, yCursor, charEndsAt, charStartsAt, atOnce);  //draw character using parsed width
    if (!atOnce) {
      sendFrameBuffer(framebuffer, SIZE_OF_FRAMEBUFFER);
    }
    delay(charDelay);
  }
  if (atOnce) {
    sendFrameBuffer(framebuffer, SIZE_OF_FRAMEBUFFER);
  }
  delay(milisNextPage);
}

/*
   Overloaded Version, starting at dot (1/1)
*/
void textPager(String text, int milisNextPage, int charDelay, bool atOnce) {
  textPager(text, milisNextPage, charDelay, atOnce, 1, 1);
}

// ===========================================================================================
/*
   Get the size of the Character to predict
   wether it fits on the screen or not...
   (Method mostly copied of wwFlip-Library-Examples)
*/
int charSize(unsigned char charCodepageID,   byte &characterStartsAt, byte &characterEndsAt,  byte fontStartsAt, byte fontEndsAt, bool reset) {
  unsigned char fontCol, fontRow, charRowValue; // vars for loop
  if (reset) {
    characterStartsAt = 15; // default max, to be overridden
    characterEndsAt = 0;    // default min, to be overridden
  }

  // parsing the characters start/end
  for (fontRow = 0; fontRow < gFontHeight; fontRow++) {
    charRowValue = pgm_read_byte(&(font[charCodepageID][fontRow]));  // read data From Progmem

    if (charRowValue != 0) { // if there's no pixel set in this row: hurry up!
      charRowValue = charRowValue >> fontStartsAt; //shift the row to the first pixel of the font...
      for (fontCol = fontStartsAt; fontCol <= fontEndsAt; fontCol++) {
        if (charRowValue & 1) { // is this bit set?
          if (characterEndsAt < fontCol) {
            characterEndsAt = fontCol;

#ifdef DEBUG_CHAR
            Serial.print("Detected new max x-Pixel: ");
            Serial.println(characterEndsAt);
#endif

          }
          if (characterStartsAt > fontCol) {
            characterStartsAt = fontCol;
#ifdef DEBUG_CHAR
            Serial.print("Detected new min x-Pixel: ");
            Serial.println(characterStartsAt);
#endif
          }
        }
        charRowValue = charRowValue >> 1; //shift the row by 1 bit (next column)
      }
    }
  }

  int myFontWidth = characterEndsAt - characterStartsAt + 1;
  if (myFontWidth < 0) {
    myFontWidth = 0;
  }
  return myFontWidth;
}

/*
   Overloaded Version of charSize
*/
int charSize(unsigned char charCodepageID,   byte &characterStartsAt, byte &characterEndsAt) {
  return charSize(charCodepageID,  characterStartsAt, characterEndsAt, 0, 15, true);
}

// ===========================================================================================
/*
   Parse the Fonts Character and set the
   dots in the Framebuffer correspondently
   (Method mostly copied of wwFlip-Library-Examples)
*/
int drawChar(unsigned char charCodepageID, int xOffs, int yOffs,  byte characterEndsAt, byte characterStartsAt, bool atOnce) {
  unsigned char fontCol, fontRow, charRowValue; // vars for loop

  for (fontRow = 0; fontRow <  gFontHeight; fontRow++) {
    charRowValue = pgm_read_byte(&(font[charCodepageID][fontRow]));  // read data From Progmem

#ifdef DEBUG_CHAR
    Serial.print("Ausgabewert: ");
    Serial.print(charRowValue, BIN);
    Serial.print(" | Starts at: ");
    Serial.print(characterStartsAt);
    Serial.print(" / Ends at: ");
    Serial.println(characterEndsAt);
#endif

    if (charRowValue != 0) { // if there's no pixel set in this row: hurry up!
      charRowValue = charRowValue >> characterStartsAt; //shift the row to the first pixel of the character...
      for (fontCol = characterStartsAt; fontCol <= characterEndsAt; fontCol++) {
        setFramebufferBit(fontCol + xOffs - characterStartsAt - 1, fontRow + yOffs - 1, charRowValue & 1);
#ifdef DEBUG_CHAR
        if (charRowValue & 1) { // is this bit set?
          Serial.print('x');
        } else {
          Serial.print(' ');
        }
#endif
        charRowValue = charRowValue >> 1; //shift the row by 1 bit (next column)
      }
    }
#ifdef DEBUG_CHAR
    Serial.println();
#endif
  }
  int myFontWidth = characterEndsAt - characterStartsAt + 1;
  if (myFontWidth < 0) {
    myFontWidth = 0;
  }
  return (xOffs + myFontWidth + 1);
}


/*
   Overloaded Version of drawChar(..)
*/
int drawChar(unsigned char charCodepageID, int xOffs, int yOffs) {
  return drawChar(charCodepageID, xOffs, yOffs, gFontStartsAt, gFontEndsAt, true);
}

// ===========================================================================================
void parseFont() {
  unsigned char fontRow, fontCol, charRowValue;
  unsigned int v;

  gFontEndsAt = 0;
  gFontStartsAt = 16;
  gFontWidth = 16;

#ifdef DEBUG_FONT
  Serial.println("Parsing the fonts:");
#endif

  gFontHeight = sizeof(font[0]) / sizeof(font[0][0]);
  gFontCharacters = sizeof(font) / sizeof(font[0]);

#ifdef DEBUG_FONT
  Serial.print("Font Height: ");
  Serial.println(gFontHeight);

  Serial.print("Character Count: ");
  Serial.println(gFontCharacters);
#endif

  // A lot Pixel are set at codepageNr. 219, letters: 'y', 'g', 'Q'
  // so let's use them to get the extreme values of the used font

  charSize(219,  gFontStartsAt, gFontEndsAt, 0, 15, false);
  charSize('y',  gFontStartsAt, gFontEndsAt, 0, 15, false);
  charSize('Q',  gFontStartsAt, gFontEndsAt, 0, 15, false);
  charSize('g',  gFontStartsAt, gFontEndsAt, 0, 15, false);

#ifdef DEBUG_FONT
  Serial.print("Fontpixel begin at bit: ");
  Serial.println(gFontStartsAt);

  Serial.print("Fontpixel end at bit: ");
  Serial.println(gFontEndsAt);
#endif

  gFontWidth = 1 + gFontEndsAt - gFontStartsAt;

#ifdef DEBUG_FONT
  Serial.print("Detected font-width: ");
  Serial.println(gFontWidth);
#endif
}

// ===========================================================================================
/*
   Some Basic Functions to set Bits
   in (Frambuffers) Byte
*/
byte setBitInByte(byte value, byte bitNr) {
  (value) |= (1UL << (bitNr));
  return value;
}

byte clearBitInByte(byte value, byte bitNr) {
  (value) &= ~(1UL << (bitNr));
  return value;
}

byte writeBitInByte(byte value, byte bitNr, bool bitvalue) {
  value = bitvalue ? setBitInByte(value, bitNr) : clearBitInByte(value, bitNr);
  return value;
}

/*
   Short-ciruit to evaluate the relevant
   bits value in (Frambuffers) Byte
*/
bool readBitInByte(byte value, byte bitNr) {
  return (((value) >> (bitNr)) & 0x01);
}

// ===========================================================================================
/*
   Basic connection to WiFi Network
*/
int connectToWIFI() {
#ifdef DEBUG_WIFI
  Serial.println("--- >8 --- connecting to WIFI --- 8< ---");
#endif
#ifdef DEBUG_WIFI_EXTREME
  Serial.setDebugOutput(true);
#endif

  WiFi.mode(WIFI_STA); //enable WIFI Station mode
  if (WIFI_HOSTNAME != "") {
    WiFi.hostname(WIFI_HOSTNAME);
  }
  WiFi.begin(WIFI_SSID, WIFI_PRESHAREDKEY);
#ifdef DEBUG_WIFI
  Serial.print(" connecting ");
#endif
  int retries = 0;
  // check the status of WiFi connection to be WL_CONNECTED
  while ((WiFi.status() != WL_CONNECTED)
         && (retries < MAX_WIFI_INIT_RETRY)) {
    retries++;
    delay(WIFI_RETRY_DELAY);
#ifdef DEBUG_WIFI
    Serial.print(".");
#endif
  }

  if ( WiFi.status() == WL_CONNECTED) {
#ifdef DEBUG
    Serial.println();
    Serial.print("Connected to ");
    Serial.print(WIFI_SSID);
    Serial.print(" with IP: ");
    Serial.println(WiFi.localIP());
    Serial.println("Use a Browser to connect to this Server!");
#endif
  } else {
#ifdef DEBUG
    Serial.print("Error connecting to: ");
    Serial.println(WIFI_SSID);
    Serial.println("You need to try it again (push the RESET-button on nodeMCU) or check the credentials in credentials.h or the WIFI-Section of Code");
#endif
  }

#ifdef DEBUG_WIFI_EXTREME
  WiFi.printDiag(Serial);
#endif
#ifdef DEBUG_WIFI
  Serial.println("--- >8 --- /connecting to WIFI --- 8< ---");
#endif
  return WiFi.status(); // return the WiFi connection status
}


/*
   Routing: Which function is called if the following URLs are requested
*/
void configurateRestServerRouting() {
  httpRestServer.on("/", HTTP_GET, get_root); /* "Homepage" - http://NODE_MCU_IP_ADDRESS/ */
  httpRestServer.on("/message", HTTP_PUT, putpost_message); /* If connected via HTTP-PUT-Request (p.e. CURL) - http://NODE_MCU_IP_ADDRESS/message */
  httpRestServer.on("/message", HTTP_POST, putpost_message); /* Form Submit-Button pressed - or connected via HTTP-POSTT-Request (p.e. CURL) - http://NODE_MCU_IP_ADDRESS/message */

}


/*
   When Hompage is called - respond with HTML & OK
*/
void get_root() {
  httpRestServer.send(200, "text/html", buildHTMLPage("Welcome!"));
}

/*
   When Form was submitted change displayed text and respond with HTML & OK
*/
void putpost_message() {
  String debugmessage = "";
  String textfieldname = "messageTextField";
  String textfieldvalue = "";
  String message = "No content submitted";

  if (httpRestServer.arg("messageTextField") != "") {
        textfieldvalue = httpRestServer.arg("messageTextField");
        
#ifdef DEBUG_HTTP
    Serial.print("Found named POST-Request Argument");
#endif

  } else {

#ifdef DEBUG_HTTP
    Serial.println("No named POST-request argument found. Parsing all arguments: ");
#endif
    for (int i = 0; i < httpRestServer.args(); i++) {
      debugmessage += " Argument " + (String)i + ": (key) ";
      debugmessage += httpRestServer.argName(i) + " = (value) ";
      debugmessage += httpRestServer.arg(i) + "\n";
      if (httpRestServer.argName(i) == "plain") {
               textfieldvalue = getValue(httpRestServer.arg(i), '=', 1); /* plain contains "key=value" pairs - use the value*/  
#ifdef DEBUG_HTTP
        Serial.print("Found POST-request argument by index");
#endif

      }
#ifdef DEBUG_HTTP
      Serial.println("Message of PUT-Requests (key/value-pair): ");
      Serial.println(debugmessage);
#endif
    }
  }

  textfieldvalue.trim();
  gTextToBeDisplayed = textfieldvalue;
  if (gTextToBeDisplayed != "" ) {
    message = "Received new Content: '" + gTextToBeDisplayed + "'";
  }
  //text = getValue(putMessageBody, '=', 1); /* Formular übertragt nach "key=value" Sytematik

  if (httpRestServer.method() == HTTP_POST) { /* no HTML-Page sent back if it was PUT */
    message = buildHTMLPage(message);
  }
  httpRestServer.send(200, "text/html", message);
}



/*
   Generate HTML-Homepage
*/
String buildHTMLPage(String message) {
  String htmlPage = "";
  htmlPage = htmlPage + "<!DOCTYPE html>" + "\r\n";
  htmlPage = htmlPage + "<html>" + "\r\n";
  htmlPage = htmlPage + "<head>" + "\r\n";
  htmlPage = htmlPage + "<title>nodeMCU-FlipDot Server</title>" + "\r\n";
  htmlPage = htmlPage + "</head>" + "\r\n";
  htmlPage = htmlPage + "<body>" + "\r\n";
  if (message != "") {
    htmlPage = htmlPage + "<h1>" + message + "</h1>" + "\r\n";
  }
  htmlPage = htmlPage + "<h1>nodeMCU-f&uuml;r das FlipDot</h1>" + "\r\n";
  htmlPage = htmlPage + "<p>Text displayed at the moment:" + "\r\n";
  htmlPage = htmlPage + String(gTextToBeDisplayed) + "</p>" + "\r\n";
  htmlPage = htmlPage + "<form action='/message' method='post' id='message' enctype='text/plain'>" + "\r\n";
  htmlPage = htmlPage + "<p>Text to be displayed:" + "\r\n";
  htmlPage = htmlPage + "<input maxlength='30' name='messageTextField' form='message'>" + "\r\n";
  htmlPage = htmlPage + "</p>" + "\r\n";
  htmlPage = htmlPage + "<button type='submit' form='message'>speichern</button>" + "\r\n";
  htmlPage = htmlPage + "</form>" + "\r\n";
  htmlPage = htmlPage + "</body>" + "\r\n";
  htmlPage = htmlPage + "</html>" + "\r\n";
  return htmlPage;
}

// https://arduino.stackexchange.com/questions/1013/how-do-i-split-an-incoming-string
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
